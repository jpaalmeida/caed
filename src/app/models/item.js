const mongoose = require('../../database');

const ItemSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true,
        unique: true
    },
    item: {
        type: String,
        required: true,
    },
    referencia: {
        type: String,
        required: true,
    },
    sequencial: {
        type: String,
        required: true,
    },
    solicitacao: {
        type: String,
        required: true,
    },
    chaves: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Chave',
        required: true,
    }],
    ordem: {
        type: Number,
        required: true,
        unique: true
    },
    situacao: {
        type: String,
        required: true,
        enum: ['DISPONIVEL', 'RESERVADA', 'CORRIGIDA', 'COM_DEFEITO'],
        default: 'DISPONIVEL'
    },
    createdAt: {
        type: Date,
        default: Date.now,
    }
});

const Item = mongoose.model('Item', ItemSchema);

module.exports = Item;