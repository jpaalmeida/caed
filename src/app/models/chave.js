const mongoose = require('../../database');

const ChaveSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: true,
        unique: true
    },
    titulo: {
        type: String
    },
    opcoes: [{
            valor: {
                type: String,
                required: true
            },
            descricao: {
                type: String
            }            
    }],
    createdAt: {
        type: Date,
        default: Date.now,
    }
});

const Chave = mongoose.model('Chave', ChaveSchema);

module.exports = Chave;