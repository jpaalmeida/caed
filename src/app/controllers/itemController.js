const express = require('express');
//const authMiddleware = require('../middlewares/auth');

const Item = require('../models/Item');
const Chave = require('../models/Chave');

const router = express.Router();

//router.use(authMiddleware);
/*
router.get('/', async (req, res) => {
    try {
        const projects = await Project
        .find()
        .populate('user');

        return res.send({ projects });
    } catch (err) {
        return res.status(400).send({ error: 'Error loading projects' });
    }
});
*/
router.get('/proxima', async (req, res) => {
    try {
        const item = await Item
        .findOne({ situacao: ['DISPONIVEL', 'RESERVADA'] })
        .sort('ordem')
        .populate({ path: 'Chaves', select: 'titulo' });

        if (!item)
            return res.send({ data: null, situacao: "ERRO", tipo: "SEM_CORRECAO", descricao: "Não existem mais correções disponíveis" });  

        return res.status(200).send({ item });

    } catch (err) {
        return res.status(400).send({ error: 'Error loading correction' });
    }
});

router.post('/', async (req, res) => {
    try {
        const { id, ordem } = req.body;

        if (await Item.findOne( { ordem }))
            return res.status(400).send({ error: 'Order already exists' });

        if (await Item.findOne( { id }))
            return res.status(400).send({ error: 'Id already exists' })    

        const item = await Item.create(req.body);

        return res.send({ item });
    } catch (err) {
        return res.status(400).send({ error: 'Error creating new project' });
    }
});

/*
* Reservadas
*/
router.get('/reservadas', async (req, res) => {
    try {
        const item = await Item
        .find({ situacao: 'RESERVADA' })
        .sort('ordem')
        .populate('Chaves');

        if (!item)
            return res.status(404).send({ data: null, situacao: "ERRO", tipo: "SEM_CORRECAO", descricao: "Não existem mais correções reservadas" });
      
        return res.status(200).send({ item });

    } catch (err) {
        return res.status(400).send({ error: 'Error loading correction' });
    }
});

router.get('/:idCorrecao', async (req, res) => {
    try {
        const idCorrecao = req.params.idCorrecao;
        
        const item = await Item
        .find({id: idCorrecao})
        .sort('ordem')
        .populate({ path: 'Chaves', select: 'titulo' });

        if (!item)
            return res.send({ data: null, situacao: "ERRO", tipo: "SEM_CORRECAO", descricao: "Não existem mais correções disponíveis" });

        return res.status(200).send({ item });

    } catch (err) {
        return res.status(400).send({ error: 'Error loading correction' });
    }
});
/*
router.put('/:projectId', async (req, res) => {
    res.send({ user: req.userId });
});

/*
router.post('/', async (req, res) => {
    try {
        const {title, description, tasks } = req.body;

        const project = await Project.create({ title, description, user: req.userId });
        //const project = await Project.create({ ...req.body, user: req.userId });

        await Promise.all(tasks.map(async task => {
            const projectTask = new Task({ ...task, project: project._id });

            await projectTask.save();
            project.tasks.push(projectTask);
        }));

        await project.save();

        return res.send({ project });
    } catch (err) {
        return res.status(400).send({ error: 'Error creating new project' });
    }
});

*/
/*
router.delete('/:projectId', async (req, res) => {
    try {
        const project = await Project.findByIdAndRemove(req.params.projectId);

        return res.send();
    } catch (err) {
        return res.status(400).send({ error: 'Error loading project' });
    }
});
*/

module.exports = app => app.use('/correcoes', router);