const express = require('express');
const authMiddleware = require('../middlewares/auth');

const Chave = require('../models/Chave');

const router = express.Router();

//router.use(authMiddleware);  /* Com autenticação */

router.get('/', async (req, res) => {
    try {
        const chaves = await Chave.find();

        return res.send({ chaves });
    } catch (err) {
        return res.status(400).send({ error: 'Error loading key correction' });
    }
});

router.post('/', async (req, res) => {
    try {
        const { id } = req.body;

        if (await Chave.findOne( { id }))
            return res.status(400).send({ error: 'Key already exists' })

        const chaves = await Chave.create(req.body);

        return res.send({ chaves });
    } catch (err) {
        return res.status(400).send({ error: 'Error creating new key correction' });
    }
});

module.exports = app => app.use('/chaves', router);
