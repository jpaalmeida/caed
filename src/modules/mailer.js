const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const path = require('path');

//const mailConfig = require('../config/mailer.js'); /* OU */ opção 1
const { host, port, user, pass } = require('../config/mailer.json');

/* opção 1
const transport = nodemailer.createTransport({
    host: "",
    port: ,
    auth: {
      user: "",
      pass: ""
    }
  });
  */

 const transport = nodemailer.createTransport({
    host,
    port,
    auth: {
      user,
      pass
    }
  });

  
transport.use('compile', hbs( {
    viewEngine: 'handlebars',
    viewPath: path.resolve('./src/resources/mail/'),
    extName: '.html'
}));

  module.exports = transport;