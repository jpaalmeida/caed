require("dotenv").config({
  path: process.env.NODE_ENV === "test" ? ".env.test" : ".env"
});

const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

mongoose.connect('mongodb://'+process.env.DB_HOST+'/'+process.env.DB_NAME, {useNewUrlParser: true, useUnifiedTopology: true}).then(connection => {
    //console.log('Connected to MongoDB DB')
  })
  .catch(error => {
  console.log(error.message)
  }); //*** useMongoClient similar ao PDO ***

module.exports = mongoose;