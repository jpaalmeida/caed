const bodyParse = require('body-parser');
const app = require("./app");

app.use(bodyParse.json());

app.use(bodyParse.urlencoded({ extended: false }));

require('./app/controllers/index')(app); //Repassando o app -> objeto definido uma vez e vai ser usado em todos os arquivos

app.listen(3000);