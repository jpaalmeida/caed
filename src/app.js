require("dotenv").config({
  path: process.env.NODE_ENV === "test" ? ".env.test" : ".env"
});

const express = require("express");
const bodyParse = require('body-parser');


class AppController {
  constructor() {
    this.express = express();
    this.bodyParse = bodyParse();

    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.express.use(express.json());
  }

  routes() {
    this.express.use(require("./routes"));
  }

}

module.exports = new AppController().express;
