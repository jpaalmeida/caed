## API NODE.JS

-> Executar o src/server.js

-> Banco de dados MongoDB (HOST=localhost / DBNAME=caed) setado nas variáveis de ambiente dentro do arquivo .env.

**Rotas**:

1. GET => http://localhost:3000/chaves.

2. POST => http://localhost:3000/chaves.

3. POST => http://localhost:3000/correcoes.

4. GET => http://localhost:3000/correcoes/reservadas.

5. GET => http://localhost:3000/correcoes/proxima.

6. GET => http://localhost:3000/correcoes/:id.